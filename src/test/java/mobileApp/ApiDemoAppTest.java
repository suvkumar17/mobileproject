package mobileApp;

import pageObjects.DragAndDropPage;
import pageObjects.HomePage;
import pageObjects.Preference;
import pageObjects.ViewsPage;

import java.io.IOException;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionUtilities;
import utilities.Base;

public class ApiDemoAppTest extends Base {
	
	
	@Test(priority= 1)
	public void apiDemoTest() throws IOException, InterruptedException {
		
		System.out.println("checking testing ***********");

//Start Appium server if not started	
//	startService();	
	
	AndroidDriver<AndroidElement> driver = capabilities();
//	startService();
	HomePage homePage = new HomePage(driver);
	
	
/*	Actions action = new Actions(driver);
	action.moveToElement(homePage.preference);
	action.doubleClick();
	action.build().perform();
*/	
	homePage.preference.click();
	
	Preference preferenceObj = new Preference(driver);
	preferenceObj.switches.click();
	
	if (preferenceObj.prefRadiobtn.isSelected()== false) {
//	System.out.println(homePage.switching1.getText());
	preferenceObj.prefRadiobtn.click();
	System.out.println(preferenceObj.prefRadiobtn.getText());
	}
	
	if (preferenceObj.prefCheckbx.isSelected()== false) {
		preferenceObj.prefCheckbx.click();
	}
	driver.navigate().back();
//	homePage.accessibility.click();
	driver.navigate().back();
//	homePage.views.click();
	
	
//Tap on an element	
//	ViewsPage viewsPage = new ViewsPage(driver);
//	ActionUtilities act = new ActionUtilities(driver);
//	act.tapOn(viewsPage.expandableLists);
//	driver.navigate().back(); 
	//service.stop();
	
//	TouchAction ta = new TouchAction(driver);
//	ta.tap(ElementOption.element(viewsPage.expandableLists)).perform();
	
//Scrolling up and down
/*	act.scrollUp();
	Thread.sleep(1000);
	act.scrollDown();
*/
	
//Swipe Left and Right
/*		act.swipeLeft();
		Thread.sleep(1000);
		act.swipeRight();
*/
		
//Drag and drop on an element
/*	act.tapOn(viewsPage.dragAnddrop);
	DragAndDropPage dgPage = new DragAndDropPage(driver);
	act.dragAndDropElement(dgPage.sourceElement, dgPage.destinationElement);

	driver.navigate().back();
	driver.navigate().back();
	homePage.preference.click();
	driver.findElementByXPath("//*[@text='9. Switch']").click();;
	
	String b = driver.findElementById("android:id/checkbox").getAttribute("checked");
	if(b.equalsIgnoreCase("false")) {
		driver.findElementById("android:id/checkbox").click();
	}
*/	
  
//To lock and unlock the device
//	driver.lockDevice();
//	driver.unlockDevice();
	
	
//	TouchAction t = new TouchAction(driver);
//	t.longPress(ElementOption.element(element1)).moveTo(ElementOption.element(element2)).release().perform();
	
//	act.scrollToText(viewsPage.webView);
//	 driver.findElement(MobileBy.AndroidUIAutomator(
//				"new UiScrollable(new UiSelector()).scrollIntoView(text(\"WebView\"))"));

	//To check properties
/*	List <MobileElement> prop = (List<MobileElement>)  driver.findElementByAndroidUIAutomator("new UiSelector().clickable(true)");
	MobileElement s = prop.get(0);
	System.out.println(s.getText());
*/	
	}
	
	
	
		
		
	
}