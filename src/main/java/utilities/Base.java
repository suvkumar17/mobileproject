package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class Base {

//	public static AppiumDriverLocalService service;
	public static AndroidDriver<AndroidElement> driver;
	
//To start the appium server if not started already
/*	public AppiumDriverLocalService startServer()
	{
		
	boolean flag = checkIfServerIsRunnning(4723);
	if(!flag)
	{
		
		service=AppiumDriverLocalService.buildDefaultService();
		service.start();
	}
		return service;
		
	}
	
	public static boolean checkIfServerIsRunnning(int port) {		
		
		boolean isServerRunning = false;
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(port);
			
			serverSocket.close();
		} catch (IOException e) {
			//If control comes here, then it means that the port is in use
			isServerRunning = true;
		} finally {
			serverSocket = null;
		}
		return isServerRunning;
	}
*/	
/*	public void startService() {
		AppiumDriverLocalService service;
		AppiumServiceBuilder builder = new AppiumServiceBuilder();
		builder.withIPAddress("127.0.0.1");
		builder.usingPort(4724);
		service = AppiumDriverLocalService.buildService(builder);
	    builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
	    builder.withArgument(GeneralServerFlag.LOG_LEVEL,"error");
		service.start();
		//return service;
	}
	
*/	
	
	public static  AndroidDriver<AndroidElement> capabilities() throws IOException, InterruptedException
		{
		
		
		//Loading the properties file
		FileInputStream file = new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\java\\utilities\\global.properties");
		Properties properties = new Properties();
		properties.load(file);
		String device=(String) properties.get("deviceName");
		String appsPackage = (String) properties.getProperty("appsPackage");
		String activity = (String) properties.getProperty("appsActivity");
		String appName = (String) properties.getProperty("appsName");
		
		
		//App path
		 File appDir = new File("src");
	     File app = new File(appDir, (String) properties.get(appName));
		
		// Setting up desired capabilities
	    DesiredCapabilities capabilities = new DesiredCapabilities();
	    
	 	// To send the device name from command prompt (mvn test -deviceName=mi)
	//   String device = (String)System.getProperty("deviceName");
	   
	    //start the emulator
	    if(device.contains("Emulator")) {
	    	startEmulator();	
	    }
	    
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device);
	    capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
	    capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,14);
	    capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
	   // capabilities.setCapability("appPackage", appsPackage);
	 	//capabilities.setCapability("appActivity",activity);
	    capabilities.setCapability("noReset", "false");
	    
	    //Initializing driver
	    driver = new AndroidDriver<>(new URL("http://127.0.0.1:4724/wd/hub"), capabilities);
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver; 
		}
	
	public static void startEmulator() throws IOException, InterruptedException {
		Runtime.getRuntime().exec(System.getProperty("usr.dir")+"\\src\\main\\java\\resources\\StartEmulator.bat");
		Thread.sleep(6000);
	}
	
	public static void getScreenshot(String s) throws IOException
	{
	File scrfile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(scrfile,new File(System.getProperty("user.dir")+"\\"+s+".png"));
	
	}
	
}
