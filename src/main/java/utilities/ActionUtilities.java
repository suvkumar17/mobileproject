package utilities;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class ActionUtilities {
	AndroidDriver<AndroidElement> driver ;
	
	public ActionUtilities(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
//Tap action
	public void tapOn(MobileElement expandableLists) {
	TouchAction t = new TouchAction(driver);
	t.tap(ElementOption.element(expandableLists)).perform();
	}

//Drag and Drop
	public void dragAndDropElement(MobileElement source, MobileElement destination) {
	TouchAction t = new TouchAction(driver);
	t.longPress(ElementOption.element(source)).moveTo(ElementOption.element(destination)).release().perform();
	}	

//Scrolling to particular element
	public void scrollToText(MobileElement webView) {
		String s = webView.toString().trim();
		driver.findElement(MobileBy.AndroidUIAutomator(
				"new UiScrollable(new UiSelector()).scrollIntoView(text(\""+s+"\"))"));	
	}
	
//Scrolling and Swipe	
    int startX = 0;
    int endX = 0;
    int startY = 0;
    int endY = 0;
	
    public void scrollUp() {		
		Dimension dimension = driver.manage().window().getSize();
	 //   int deviceHeight = dimension.getHeight();
	 //   int deviceWidth = dimension.getWidth();
		startY = (int) (dimension.height * 0.70);
		endY = (int) (dimension.height * 0.30);
		startX = (dimension.width / 2);
	    
	    TouchAction action = new TouchAction(driver);
	    action.longPress(PointOption.point(startX, startY)).moveTo(PointOption.point(startX, endY)).release().perform();
	}
    
    public void scrollDown() {	
    	Dimension dimension = driver.manage().window().getSize();
		endY = (int) (dimension.height * 0.70);
		startY = (int) (dimension.height * 0.30);
		startX = (dimension.width / 2);
	    
	    TouchAction action = new TouchAction(driver);
	    action.longPress(PointOption.point(startX, startY)).moveTo(PointOption.point(startX, endY)).release().perform();
	}
    
    public void swipeLeft() {	
    	Dimension dimension = driver.manage().window().getSize();
    	startY = (int) (dimension.height / 2);
    	startX = (int) (dimension.width * 0.90);
    	endX = (int) (dimension.width * 0.05);
	    
	    TouchAction action = new TouchAction(driver);
	    action.longPress(PointOption.point(startX, startY)).moveTo(PointOption.point(startX, endY)).release().perform();
	}
	
    public void swipeRight() {	
    	Dimension dimension = driver.manage().window().getSize();
    	startY = (int) (dimension.height / 2);
    	startX = (int) (dimension.width * 0.05);
    	endX = (int) (dimension.width * 0.90);
	    
	    TouchAction action = new TouchAction(driver);
	    action.longPress(PointOption.point(startX, startY)).moveTo(PointOption.point(startX, endY)).release().perform();
	    
	}
    

}