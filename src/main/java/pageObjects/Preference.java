package pageObjects;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Preference {

	public Preference(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(accessibility="9. Switch")
	public MobileElement switches;
	
	@AndroidFindBy(id="android:id/switch_widget")
	public MobileElement prefRadiobtn;
	
	@AndroidFindBy(id="android:id/checkbox")
	public MobileElement prefCheckbx;
}