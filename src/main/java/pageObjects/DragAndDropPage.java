package pageObjects;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DragAndDropPage {
	
	public DragAndDropPage(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);	
	}
	
	@AndroidFindBy(id="io.appium.android.apis:id/drag_dot_1")
	public MobileElement sourceElement;
	
	@AndroidFindBy(id="io.appium.android.apis:id/drag_dot_3")
	public MobileElement destinationElement;

}
