package pageObjects;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ViewsPage {

		public ViewsPage(AndroidDriver<AndroidElement> driver) {
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);	
		}
		
		@AndroidFindBy(xpath="//android.widget.TextView[@text='WebView']")
		public MobileElement webView;
		
		@AndroidFindBy(xpath="//android.widget.TextView[@text='Expandable Lists']")
		public MobileElement expandableLists;
		
		@AndroidFindBy(xpath="//android.widget.TextView[@text='Drag and Drop']")
		public MobileElement dragAnddrop;
		
}

